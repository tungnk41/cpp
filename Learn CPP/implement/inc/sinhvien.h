#ifndef SINHVIEN_H
#define SINHVIEN_H

#include <iostream>
#include <string>

class SinhVien
{
	private:
	int m_id;
	std::string m_name;
	
	public:
	SinhVien(){}
	SinhVien(int id,std::string name) : m_id(id),m_name(name){}
	SinhVien(SinhVien& sv) : m_id(sv.getID()), m_name(sv.getName()){}
	SinhVien& operator=(SinhVien& sv);
	
	void setName(std::string name);
	std::string getName();
	void setID(int id);
	int getID();
	void showInformation();
};

#endif
