#ifndef QUEUE_H
#define QUEUE_H

#include <string>
#include <iostream>
#include "../inc/sinhvien.h"
#define MAX 5

class Queue
{
	private:
		int front,rear,size; //rear :  chi so cua phan tu cuoi cung trong queue, font : chi so cua phan tu dau tien trong queue khi moi them vao
		SinhVien sv[MAX]; //Mang Queue chua cac phan tu Sinh vien
	public:
		//Khoi tao gia tri 
		Queue()
		{
			front = 0;
			rear = -1;
			size = 0;
			for(int i=0;i<MAX;i++)
			{
				sv[i].setID(0);
				sv[i].setName("");
			}
		}
		bool isFull();
		bool isEmpty();
		void push_back(SinhVien sv);
		SinhVien dequeue();
		SinhVien* head();
		int getSize();
		int getCapacity();
		void print();
		void remove_back();
		void push_front(SinhVien sv);
		
};

	
#endif 
