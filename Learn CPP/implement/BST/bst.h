#ifndef BST_H
#define BST_H

#include <iostream>
#include <string>
#include "../inc/sinhvien.h"


class Node
{
	private:
		SinhVien m_sv;
		Node* m_left;
		Node* m_right;
	
	public:
		Node(): m_sv(0,""),m_left(NULL),m_right(NULL){}
		Node(SinhVien sv) : m_sv(sv),m_left(NULL),m_right(NULL){}
	
		void setData(SinhVien sv);
		SinhVien& getData();
		void setLeft(Node* left);
		Node* getLeft();
		void setRight(Node* right);
		Node* getRight();	
};


class BST
{
	private:
		Node* m_root;
		void insert(Node* root,SinhVien sv);
		void deleteBST(Node* root);
		void preOrder(Node* root);
		void inOrder(Node* root);
		void postOrder(Node* root);
		void find(Node* root,int id);
		void find(Node* root,std::string name);
	public:
		
		BST() : m_root(NULL){}
		
		void insert(SinhVien sv);
		void deleteBST();
		void preOrder();
		void inOrder();
		void postOrder();
		void find(int id);
		void find(std::string name);
		~BST();
};

#endif
