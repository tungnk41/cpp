#include "bst.h"

void Node::setData(SinhVien sv){
	this->m_sv = sv;
}
	
SinhVien& Node::getData(){
	return this->m_sv;
}

void Node::setLeft(Node* left){
	this->m_left = left;
}
	
Node* Node::getLeft(){
	return this->m_left;
}	
	
void Node::setRight(Node* right){
	this->m_right = right;
}
	
Node* Node::getRight(){
	return this->m_right;
}


		
void BST::insert(SinhVien sv){
	insert(m_root,sv);
}
		
void BST::deleteBST(){
	BST::deleteBST(m_root);
}
		
void BST::preOrder(){
	BST::preOrder(m_root);
}
		
void BST::inOrder(){
	BST::inOrder(m_root);
}
		
void BST::postOrder(){
	BST::postOrder(m_root);
}
		
void BST::find(int id){
	BST::find(m_root,id);
}
		
void BST::find(std::string name){
	BST::find(m_root,name);
}	
		
BST::~BST(){
	BST::deleteBST();
}



void BST::insert(Node* root,SinhVien sv){
	if(root == NULL){
		m_root = new Node(sv);
	}
	else{
		if ( sv.getID() < root->getData().getID() ){
			if(root->getLeft() == NULL){
				Node* temp = new Node(sv);
				root->setLeft(temp);	 
			}
			else{

				insert(root->getLeft(),sv);
			}
		}
		else {
			if(root->getRight() == NULL){

				Node* temp = new Node(sv);
				root->setRight(temp);
			}
			else{
				insert(root->getRight(),sv);
			}
		}
	}
		
}

void BST::deleteBST(Node* root){
	if(root == NULL){
		return;
	}
	Node* currentNode = root;
	Node* leftNode = root->getLeft();
	Node* rightNode = root->getRight();
	delete currentNode;
	deleteBST(leftNode);
	deleteBST(rightNode);
}

void BST::preOrder(Node* root){
	if(root == NULL){
		return;
	}
	else{
		std::cout<<"ID   : "<<root->getData().getID()<<std::endl;
		std::cout<<"Name : "<<root->getData().getName()<<std::endl;
		preOrder(root->getLeft());
		preOrder(root->getRight());
	}
}

void BST::inOrder(Node* root){
	if(root == NULL){
		return;
	}
	else{
		inOrder(root->getLeft());
		std::cout<<"ID   : "<<root->getData().getID()<<std::endl;
		std::cout<<"Name : "<<root->getData().getName()<<std::endl;
		inOrder(root->getRight());
	}
}

void BST::postOrder(Node* root){
	if(root == NULL){
		return;
	}
	else{
		postOrder(root->getLeft());
		postOrder(root->getRight());
		std::cout<<"ID   : "<<root->getData().getID()<<std::endl;
		std::cout<<"Name : "<<root->getData().getName()<<std::endl;	
	}
}

void BST::find(Node* root,int id){
	if(root == NULL){
		return;
	}
	else{
		if(id == root->getData().getID() ){
				std::cout<<"Name : " <<root->getData().getName()<<std::endl;
		}
		if (id < root->getData().getID() ){
				find(root->getLeft(),id);
		}
		if(id > root->getData().getID() ){
				find(root->getRight(),id);
		}	
	}	
}


void BST::find(Node* root,std::string name){
	
	if(root == NULL){
		return;
	}
	else{
		if(name == root->getData().getName() ){
			std::cout<<"ID : " <<root->getData().getID()<<std::endl;
		}
		find(root->getLeft(),name);
		find(root->getRight(),name);	
	}	
}
