#include "stack.h"


Stack::Stack()
		{
			m_top = -1;
			for(int i=0;i<MAX;i++)
			{
				sv[i].setID(0);
				sv[i].setName("");
			}
		}

void Stack::push(SinhVien sv)
{
	//Neu Stack da dien day toi phan tu thu MAX-1 
	if(m_top>=MAX-1)
	{
		std::cout<<"Stack Overflow"<<std::endl;
	}
	else
	{
		m_top += 1;
		//update gia tri phan tu taidinh Stack
		this->sv[m_top] = sv;
	}
}

void Stack::pop()
{
	if(m_top<0)
	{
		std::cout<<"Stack Underflow"<<std::endl;
	}
	else
	{
		//Sau khi pop phan tu tai dinh  Stack thi xoa bo phan tu do ra khoi stack bang cach gan ID = 0, name = ""
		sv[m_top].setID(0);
		sv[m_top].setName("");
		//update lai vi tri con tro tro toi dinh Stack
		m_top--;
	}
}


//Tra ve gia tri phan tu tai dinh stack nhung ko xoa phan tu do
SinhVien& Stack::top()
{
	//Neu con tro tai dinh Stack nam trong khoang gia tri hop le 
	//thi tuc la Stack dang co Phan tu trong do
	if(m_top>=0 && m_top <= MAX-1)
		return this->sv[m_top];
	//Sau khi pop phan tu tai vi tri 0 (da pop het hoan toan gia tri co trong Stack)
	//Ta tra ve gia tri la 1 bien SinhVien rong~
	else if(m_top == -1)
		return sv[0];
}

