#ifndef STACK_H
#define STACK_H

#include <iostream>
#include "../inc/sinhvien.h"

#define MAX 10

class Stack
{
	private:
		// m_top : chi so Mang cua stack chua vi tri phan tu o dinh Stack
		int m_top;
		SinhVien sv[MAX];
	public:
		//Khoi tao cac phan tu Sinh Vien ban dau co ID = 0, name =""
		Stack();
		//Them phan tu Sinh Vien vao dau Stack
		void push(SinhVien sv);
		//Remove phan tu Sinh vien o dau Stack
		void pop();
		SinhVien& top();
		
};

#endif
