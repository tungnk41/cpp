#include "hash.h"
HashMap::HashMap()
{
    //Khoi tao HashMap voi cac phan tu la con tro HashEntry = NULL
	table = new HashEntry* [TABLE_SIZE];
	for (int i = 0; i< TABLE_SIZE; i++){
            table[i] = NULL;
    }
}

//Tao ra cac Index cua HashEntry trong bang HashMap
int HashMap::HashFunc(int key)
{
    return key % TABLE_SIZE;
}       

     
void HashMap::insert(SinhVien sv)
{
    		//Tao ra gia tri Index tu ID cua SinhVien
	int index = HashFunc(sv.getID());
           
            //Lap toi khi tim thay phan tu da ton tai co gia tri = SV them vao hoac NULL
    while (table[index] != NULL && table[index]->m_key != sv.getID()){
            //Update lai gia tri index o vi tri lien ke ke tiep 
            index = HashFunc(index + 1);
    }
        
    if (table[index] != NULL) {
    	delete table[index];	
	}
                
    table[index] = new HashEntry(sv);
}


void HashMap::search(int key)
{
    int  index = HashFunc(key);
    while (table[index] != NULL && table[index]->m_key != key){
            index = HashFunc(index + 1);
    }
    
    if (table[index] == NULL){
    	std::cout<<" Not Valid member "<<std::endl;
	}
    else{
    	std::cout<<"ID  : "<<key<<std::endl;
        std::cout<<"Name: "<<table[index]->m_value<<std::endl;
    }
           
}

HashMap::~HashMap()
{
    for (int i = 0; i < TABLE_SIZE; i++){
        if (table[i] != NULL){
        	delete table[i];
		}
    }
    
    delete[] table;
}

