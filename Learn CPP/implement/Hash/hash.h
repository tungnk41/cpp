#ifndef HASH_H
#define HASH_H

#include <iostream>
#include <string>
#include "../inc/sinhvien.h"
#define TABLE_SIZE 10

class HashEntry
{
    public:
        int m_key;
        std::string m_value;
        HashEntry() : m_key(0),m_value(""){}
        HashEntry(SinhVien sv) : m_key(sv.getID()),m_value(sv.getName()){}
};


class HashMap
{
	private:
    	//Tao bang HashMap chua cac phan tu HashEntry
        HashEntry **table;
  public:
  			HashMap();
  			int HashFunc(int key);
  			void insert(SinhVien sv);
  			void search(int key);
  			~HashMap();
};

#endif 
