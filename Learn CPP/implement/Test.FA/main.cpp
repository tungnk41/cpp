#include <iostream>
#include <string>
#include <list>
#include <memory>
#include <fstream>
#include <algorithm>
#include <cstdlib>
using namespace std;


class NhanVien
{
	protected:
		int m_id;
		string m_name;
		string m_department;
		
	public:
		NhanVien() : m_id(0),m_name(""),m_department(""){}
		enum jobTitle{
			DEVELOPER = 1,
			TESTER,
			MANAGER,
		};
		void setID(int id){
			this->m_id = id;
		} 
		
		int getID(){
			return m_id;
		}
		
		void setName(string name){
			this->m_name = name;
		}
		
		string getName(){
			return this->m_name;
		}
		
		string getDepartment(){
			return this->m_department;
		}
		
		void setDepartment(string department){
			this->m_department = department;
		}
	
		
		void virtual showInformation(){};
		void virtual displayDailyWork() = 0;
};

class Developer : public NhanVien
{
	private:
		string m_project;
		
	public:
		Developer() : NhanVien(){}

		void showInformation(){
			cout<<"ID : "<<this->m_id<<"  |  ";
			cout<<"Name : "<<this->m_name<<"  |  ";
			cout<<"Department : "<<this->m_department<<"  |  ";
			cout<<"Working in project : "<<this->m_project<<endl;
		}
		void displayDailyWork(){
			cout<<"Develop Project "<<endl;
		}
		
		void setProject(string project){
			this->m_project = project;
		}
		
		string getProject(){
			return this->m_project;
		}
		
};


class Tester : public NhanVien
{
	private:
		string m_project;
		
	public:
	    Tester() :  NhanVien(){}
		void showInformation(){
			cout<<"ID : "<<this->m_id<<"  |  ";
			cout<<"Name : "<<this->m_name<<"  |  ";
			cout<<"Department : "<<this->m_department<<"  |  ";
			cout<<"Working in project : "<<this->m_project<<endl;
		}
		void displayDailyWork(){
			cout<<"Test Project "<<endl;
		}
		
		void setProject(string project){
			this->m_project = project;
		}
		
		string getProject(){
			return this->m_project;
		}
		
};


class Manager : public NhanVien
{
	private:
		string m_customer;
		
	public:
	Manager() :  NhanVien(){}
		void showInformation(){
			cout<<"ID : "<<this->m_id<<"  |  ";
			cout<<"Name : "<<this->m_name<<"  |  ";
			cout<<"Department : "<<this->m_department<<"  |  ";
			cout<<"Customer : "<<this->m_customer<<endl;
		}
		void displayDailyWork(){
			cout<<"Working with Customer "<<endl;
		}
		
		void setCustomer(string name){
			this->m_customer = name;
		}
		
		string getCustomer(){
			return this->m_customer;
		}
	
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool compareName(shared_ptr<NhanVien> a,shared_ptr<NhanVien> b){
	return a->getName() < b->getName();
}

bool compareID(shared_ptr<NhanVien> a,shared_ptr<NhanVien> b){
	return a->getID() < b->getID();
}


void print(list<shared_ptr<NhanVien> >& list){
	for(auto it = list.begin();it != list.end();++it){
		(*it)->showInformation();
	}
}
void input(list<shared_ptr<NhanVien> >& list,fstream& file){
	int id,jobTitle;
	string name,department,project,customer;
	while(true){
		file>>jobTitle;
		if(file.eof())break;
		switch(jobTitle){
			case NhanVien::jobTitle::DEVELOPER : {
				file>>id>>name>>department>>project;
				shared_ptr<Developer> emp = make_shared<Developer>();
				emp->setID(id);
				emp->setName(name);
				emp->setDepartment(department);
				emp->setProject(project);
				list.push_front(emp);
				break;
			}
			case NhanVien::jobTitle::TESTER : {
				file>>id>>name>>department>>project;
				shared_ptr<Tester> emp = make_shared<Tester>();
				emp->setID(id);
				emp->setName(name);
				emp->setDepartment(department);
				emp->setProject(project);
				list.push_front(emp);
				break;
			}
			case NhanVien::jobTitle::MANAGER : {
				file>>id>>name>>department>>customer;
				shared_ptr<Manager> emp = make_shared<Manager>();
				emp->setID(id);
				emp->setName(name);
				emp->setDepartment(department);
				emp->setCustomer(customer);
				list.push_front(emp);
				break;
			}
		}
	};
}

void add(list<shared_ptr<NhanVien> >& list,fstream& file){
	int num;
	int id,jobTitle;
	string name,department,project,customer;
	file.open("data.txt",ios::app);
	cout<<"Add Employee : "<<endl;
	cout<<"1. Developer "<<endl;
	cout<<"2. Tester "<<endl;
	cout<<"3. Manager "<<endl;
	cout<<"4. Exit"<<endl;
	cin>>num;
	switch(num){
		case NhanVien::jobTitle::DEVELOPER : {
			shared_ptr<Developer> emp = make_shared<Developer>();
			cout<<"ID : ";
			cin>>id;
			cout<<"Name : ";
			cin>>name;
			cout<<"Department : ";
			cin>>department;
			cout<<"Working in Project : ";
			cin>>project;
			
			emp->setID(id);
			emp->setName(name);
			emp->setDepartment(department);
			emp->setProject(project);
			list.push_front(emp);
			//file<<NhanVien::jobTitle::DEVELOPER<<" "<<id<<" "<<name<<" "<<department<<" "<<project<<endl;
			break;
		}
		case NhanVien::jobTitle::TESTER : {
			shared_ptr<Tester> emp = make_shared<Tester>();
			cout<<"ID : ";
			cin>>id;
			cout<<"Name : ";
			cin>>name;
			cout<<"Department : ";
			cin>>department;
			cout<<"Working in Project : ";
			cin>>project;
			
			emp->setID(id);
			emp->setName(name);
			emp->setDepartment(department);
			emp->setProject(project);
			list.push_front(emp);
			//file<<NhanVien::jobTitle::TESTER<<" "<<id<<" "<<name<<" "<<department<<" "<<project<<endl;
			break;
		}
		case NhanVien::jobTitle::MANAGER : {
			shared_ptr<Manager> emp = make_shared<Manager>();
			cout<<"ID : ";
			cin>>id;
			cout<<"Name : ";
			cin>>name;
			cout<<"Department : ";
			cin>>department;
			cout<<"Working with Customer : ";
			cin>>customer;
			
			emp->setID(id);
			emp->setName(name);
			emp->setDepartment(department);
			emp->setCustomer(customer);
			list.push_front(emp);
			//file<<NhanVien::jobTitle::MANAGER<<" "<<id<<" "<<name<<" "<<department<<" "<<customer<<endl;
			break;
		}
		default :{
			break;
		}
	}
}

void remove(list<shared_ptr<NhanVien> >& list){
	int id;
	cout<<" Enter the ID of Employee to remove : ";
	cin>>id;
	for(auto it = list.begin();it != list.end();++it){
		if((*it)->getID() == id){
			list.erase(it);
			break;
		}
	}
	
}

void sortID(list<shared_ptr<NhanVien> >& list){
	list.sort(&compareID);
}
void sortName(list<shared_ptr<NhanVien> >& list){
	list.sort(&compareName);
}
void find(list<shared_ptr<NhanVien> >& list){
	int id;
	cout<<"Enter the ID of Employee : ";
	cin>>id;
	for(auto it = list.begin();it != list.end();++it){
		if((*it)->getID() == id){
			(*it)->showInformation();
			break;
		}
	}
}
void run(list<shared_ptr<NhanVien> >& list,fstream& file){
	int num;
	input(list,file);
	sortName(list);
	while(true){
		cout<<endl<<endl<<endl<<endl;
		cout<<"1. Print list "<<endl;
		cout<<"2. Add Employee "<<endl;
		cout<<"3. Remove Employee "<<endl;
		cout<<"4. Sort by ID "<<endl;
		cout<<"5. Find Employee "<<endl;
		cout<<"6. Exit "<<endl;
		cin>>num;
		switch(num){
			case 1 :{
				system("cls");
				print(list);
				break;
			}
			case 2 :{
				system("cls");
				add(list,file);
				break;
			}
			case 3 : {
				system("cls");
				remove(list);
				break;
			}
			case 4 : {
				system("cls");
				sortID(list);
				print(list);
				break;
			}
			case 5 :{
				system("cls");
				find(list);
				break;
			}
			case 6 : {
				return;
			default : {
				system("cls");
				break;
			}
		}
	}
	}
	
}



int main()
{
	fstream file;
	list<shared_ptr<NhanVien> > list;
	
	file.open("data.txt",ios::in|ios::out);
	if(!file.is_open()){
		cout<<"Can not open file !"<<endl;
		return 0;
	}
	
	
	run(list,file);
	
	file.close();
	return 0;
}
