#include <iostream>
#include <string>
#include "tinyxml2.h"

using namespace std;
using namespace tinyxml2;

/*
Root (xmi-XMI)
	subRoot  (EA-Model) Tag uml:Model

*/

#define FolderTag "uml:Package"
#define FuncTag "uml:Activity"
#define subRootTag "uml:Model"

//In ra khoang trang truoc ten cac Folder va Method
void printSpace(int size){
	for(int i=0;i<size;i++){
		cout<<"     ";
	}
}

//Check xem co phai la folder hay khong
bool isPackage(XMLElement* root){
	if(strcmp(root->Attribute("xmi:type"),FolderTag) == 0){
		return true;
	}
	return false;
}

//Check xem co phai la Method hay khong
bool isActivity(XMLElement* root){	
	if(strcmp(root->Attribute("xmi:type"),FuncTag) == 0){
		return true;
	}
	return false;
}

//In ra ten cua cac Method co tag la Activity
void printNameActivity(XMLElement* root, int size){
	int i=0;
	while(root){
		i += 1;
		printSpace(size);
		cout<<i<<".Method "<<root->Attribute("name")<<endl;  
		root = root->NextSiblingElement();
	}
	return;
}

//In ra ten cua folder co tag la Package
void printNamePackage(XMLElement* root, int size){
	printSpace(size);
	cout<<root->Attribute("name")<<endl;
	return;
}


//Kiem tra cac gia tri ma con tro root dang tro toi
void checkElement(XMLElement* root,int size){

	if(!root)return;
	
	if(isActivity(root)){
		printNameActivity(root,size);	
	}
	
	if(isPackage(root)){
		int t_size;
	
		printNamePackage(root,size);
		t_size = size + 1;
		checkElement(root->FirstChildElement(),t_size);
		
		root = root->NextSiblingElement();
		checkElement(root,size);
	}
}


XMLElement* goToSubRoot(XMLNode* root,int& size){
	XMLElement* subRoot = root->FirstChildElement();  
	while(true){
		if(!subRoot){
			cout<<" Not found "<<subRootTag<<endl;
			return NULL;
		}
		if(strcmp(subRoot->Value(),subRootTag) == 0){
			printSpace(size);
			cout<<subRoot->Attribute("name")<<endl;
			subRoot = subRoot->FirstChildElement();
			size +=1;
			break;
		}
		subRoot = subRoot->NextSiblingElement();
	}
	return subRoot;
}

int main(){
	
	int size = 0;
	XMLDocument doc;
	XMLElement* subRoot;
	XMLError result = doc.LoadFile("mock_2.xml");
	if(result != XML_SUCCESS){
		cout<<"Can not open file XML "<<endl;
		return -1;
	}
	XMLNode* root = doc.RootElement(); 
	if(!root)return -1;
	
	subRoot = goToSubRoot(root,size);  
	if(!subRoot)return -1;
	checkElement(subRoot,size);

	return 0;
}




