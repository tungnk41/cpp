#include "../inc/sinhvien.h"
#include <iostream>
using namespace std;

SinhVien& SinhVien::operator=(SinhVien& sv){
	this->m_id = sv.getID();
	this->m_name = sv.getName();
	return *this;	
}


void SinhVien::setName(std::string name){
	this->m_name = name;
}

std::string SinhVien::getName(){
	return this->m_name;
}

void SinhVien::setID(int id){
	this->m_id = id;
}

int SinhVien::getID(){
	return this->m_id;
}

void SinhVien::showInformation(){
	cout<<"ID : "<<this->m_id<<endl;
	cout<<"Name : "<<this->m_name<<endl;
}
