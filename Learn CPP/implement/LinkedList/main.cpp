#include <iostream>
#include <memory>
#include "../inc/sinhvien.h"
using namespace std;

class Node
{
	private: 
	SinhVien m_sv;
	shared_ptr<Node> m_next;
	
	public:
	Node(){
		m_sv.setID(0);
		m_sv.setName("");
		m_next = nullptr;
	}
	
	Node(SinhVien sv,shared_ptr<Node> next){
		this->m_sv = sv;
		this->m_next = next;
	}		
		
	void setData(SinhVien& sv){
		this->m_sv = sv;
	}
	SinhVien getData(){
		return this->m_sv;
	}
	void setNext(shared_ptr<Node> next){
		this->m_next = next;
	}
	shared_ptr<Node> getNext(){
		return this->m_next;
	}
};

class List
{
	private:
		shared_ptr<Node> m_head,m_tail;
		int m_size;
	public:
		List() : m_head(nullptr),m_tail(nullptr),m_size(0){}
		int getSize();
		void insert(SinhVien sv);
		void remove(int id);
		void sort();
		void find(int id);
		void print();
		void swap(shared_ptr<Node> a, shared_ptr<Node> b);
		
		
};

int List::getSize(){
	return this->m_size;
}

void List::swap(shared_ptr<Node> a, shared_ptr<Node> b){
	SinhVien sv_a,sv_b;

	
	sv_a.setID(a->getData().getID());
	sv_a.setName(a->getData().getName());
	
	sv_b.setID(b->getData().getID());
	sv_b.setName(b->getData().getName());
	
	a->setData(sv_b);
	b->setData(sv_a);
}
//add data to Head of List
void List::insert(SinhVien sv){
	if(m_size == 0){
		shared_ptr<Node> node = make_shared<Node>(sv,nullptr);
		m_head = node;
		m_tail = node;
		m_size += 1;
	}
	else{
		shared_ptr<Node> node = make_shared<Node>(sv,m_head);
		m_head = node;
		m_size += 1;
	}
	
}

void List::remove(int id){
	shared_ptr<Node> current = m_head;
	shared_ptr<Node> pcurrent = m_head;
	
	while(current->getData().getID() != id){
		pcurrent = current;
		current = current->getNext();
	}
	
	if(current == m_head){
		m_head = m_head->getNext();
		current->setNext(nullptr);
		m_size -= 1;
		return;
	}
	else if(current == m_tail){
		m_tail = pcurrent;
		m_tail->setNext(nullptr);
		m_size -= 1;
		return;
	}
	else{
		pcurrent->setNext(current->getNext());
		current->setNext(nullptr);
		m_size -= 1;
		return;
	}
}

void List::sort(){
	shared_ptr<Node> current = m_head;
	shared_ptr<Node> acurrent = m_head->getNext();
	for(int i=0;i<m_size-1;i++){
		 current = m_head;
		 acurrent = m_head->getNext();
		for(int j=0;j<(m_size-i-1);j++){
			if(current->getData().getID() > acurrent->getData().getID()){
				swap(current,acurrent);
			}
				current = acurrent;
				acurrent = acurrent->getNext();
		}	
	}
}

void List::find(int id){
	
}

void List::print(){
	while(m_head){
		m_head->getData().showInformation();
		m_head = m_head->getNext();
	}
		
}

int main(int argc, char** argv) {
	List list;
	SinhVien sv[10] = {
		{9,"a"},
		{5,"b"},
		{2,"c"},
		{6,"d"},
		{1,"e"},
		{4,"f"},
		{7,"g"},
		{3,"h"},
		{10,"i"},
		{8,"k"},
	};
	
	for(int i=0;i<10;i++){
		list.insert(sv[i]);
	}
	list.sort();
	//cout<<"Size : "<<list.getSize()<<endl;
	list.print();
	return 0;
}
