#include <iostream>
#include <string>
#include <fstream>
using namespace std;

////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Book
{
	private :
		int m_id;
		string m_name;
		string m_author;
	public:
		Book() : m_id(0),m_name(""),m_author(""){}
		Book(int id, string name, string author) : m_id(id),m_name(name),m_author(author){}
		Book(Book& book){
			this->m_id = book.getID();
			this->m_name = book.getName();
			this->m_author = book.getAuthor();
		}
		
		Book& operator=(Book& book){
			this->m_id = book.getID();
			this->m_name = book.getName();
			this->m_author = book.getAuthor();
			return *this;
		}
		
		void setID(int id);
		int getID();
		void setName(string name);
		string getName();
		void setAuthor(string author);
		string getAuthor();
		void showInformation();
};

void Book::setID(int id){
	this->m_id = id;
}
int Book::getID(){
	return this->m_id;
}
void Book::setName(string name){
	this->m_name = name;
}
string Book::getName(){
	return this->m_name;
}
void Book::setAuthor(string author){
	this->m_author = author;
}
string Book::getAuthor(){
	return this->m_author;
}

void Book::showInformation(){
	cout<<"ID : "<<m_id<<"      Name : "<<m_name<<"      Author : "<<m_author<<endl;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Node
{
	private:
		Book m_book;
		Node* m_next;
	public:
		Node() : m_book(0,"",""),m_next(nullptr){}
		Node(Book book) : m_book(book),m_next(nullptr){}
		
		void setBook(Book book);
		Book getBook();
		void setNext(Node* node);
		Node* getNext();
};

void Node::setBook(Book book){
	this->m_book = book;
}
Book Node::getBook(){
	return this->m_book;
}

void Node::setNext(Node* node){
	this->m_next = node;
}

Node* Node::getNext(){
	return this->m_next;	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
class List
{
	private:
		Node* m_head;
		Node* m_tail;
		Node* m_next;
		int m_size;
	public:
		List() : m_head(nullptr),m_tail(nullptr),m_next(nullptr),m_size(0){}
		//Them 1 phan tu book vao trong list
		void insert(Book book);
		//Tim kiem 1 book trong list qua id
		Node* find(int id);
		//In ra danh sach cac phan tu book co trong list
		void print();
		//remove 1 phan tu book trong list thong qua id book
		void remove(int id);
		//Thay noi noi dung cua 1 book da ton tai trong list bang book truyen vao thong qua id cua book trong list 
		void edit(Book book,int id);
		//In ra so phan tu hien co cua List
		int getSize();
		//Thay doi gia tri bien size cua List (dung khi import data from .txt
		void setSize(int size);
		//Export data to file
		void exportData(fstream&file);
		//Import data from file
		void importData(fstream& file);
		
		~List();
}; 

int List::getSize(){
	return this->m_size;
}

void List::setSize(int size){
	this->m_size = size;
}

void List::insert(Book book){
	if(m_size == 0){
		Node* t_node = new Node(book);
		m_head = t_node;
		m_tail = t_node;
		m_tail->setNext(nullptr);
		m_size += 1;
	}
	else{
		Node* t_node = new Node(book);
		t_node->setNext(m_head);
		m_head = t_node;
		m_size += 1;
	}
}

void List::remove(int id){
	Node* pCurrent = nullptr;
	Node* current = m_head;
	
	while(current->getBook().getID() != id){
		pCurrent = current;
		current = current->getNext();
	}
	
	if(current == nullptr){
		cout<<"Not find item"<<endl;
		return;
	}
	
	if(current == m_head){
		m_head = m_head->getNext();
		current->setNext(nullptr);
		delete current;
		m_size -= 1;
		return;
	}
	else if(current == m_tail){
		m_tail = pCurrent;
		m_tail->setNext(nullptr);
		delete current;
		m_size -= 1;
		return;
	}
	else{

		pCurrent->setNext(current->getNext());
		current->setNext(nullptr);
		delete current;
		m_size -= 1;
		return;
	}
	
	
}
Node* List::find(int id){
	Node* current = m_head;
	while(current->getBook().getID() != id){
		if(current == nullptr){
			cout<<" Invalid ID "<<endl;
		}
		else{
			current = current->getNext();
		}
		
	}
	current->getBook().showInformation();
	return current;
}



void List::print(){
	Node* current = m_head;
	while(current){
		current->getBook().showInformation();
		current = current->getNext();
	}	
}

	
void List::edit(Book book,int id){
	Node* current = m_head;
	
	int t_id = book.getID();
	string t_name = book.getName();
	string t_author = book.getAuthor();
	
	while(current->getBook().getID() != id){
		if(current == nullptr){
			cout<<"invalid ID "<<endl;
		}
		else{
			current = current->getNext();
		}
	}
	current->setBook(book);

	return;
}


void List::exportData(fstream& file){
	Node* current = m_head;
	
	
		file.open("data.txt",ios::out|ios::app);
	if(!file.is_open()){
		cout<<"Can not open File"<<endl;
		return;
	}
	

	
	file<<m_size<<endl;
	while(current){
		file<<current->getBook().getID()<<" ";
		file<<current->getBook().getName()<<" ";
		file<<current->getBook().getAuthor()<<endl;
		current = current->getNext();
	}
}

//Neu size cua list != 0, <=> list da ton tai, ko thuc hien ham import
void List::importData(fstream& file){
	int id,size;
	string name;
	string author;
	Book book;
	
	
	file.open("data.txt",ios::in);
	if(!file.is_open()){
		cout<<"Can not open File"<<endl;
		return;
	}
	
	
	if(this->m_size == 0){
		file>>size;
		setSize(size);
		
		for(int i=0;i<size;i++){
			file>>id>>name>>author;
			book.setID(id);
			book.setName(name);
			book.setAuthor(author);
			insert(book);
		}
	}
	else{
		return;
	}	
}


List::~List(){
	Node* pCurrent;
	while(m_head){
		pCurrent = m_head;
		m_head = m_head->getNext();
		delete pCurrent;
	};
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
void inputBook(List& list){
	int id;
	string name;
	string author;
	Book book;
	
	cout<<"ID of Book     : ";
	cin>>id;
	cout<<"Name of Book   : ";
	cin>>name;
	cout<<"Author of Book : ";
	cin>>author;
	book.setID(id);
	book.setName(name);
	book.setAuthor(author);
	list.insert(book);
}




int main()
{
	List list;
	fstream file;
	
	
	//Test export data to file	
	
	for(int i=0;i<2;i++){
		inputBook(list);
	}
	list.exportData(file);
	
	
	
	
	//Test import from file
	/*
		list.importData(file);
	*/
	
	list.print();
	file.close();
	return 0;
}

