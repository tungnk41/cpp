#include <iostream>
using namespace std;

#include <iostream>
using namespace std;

/*
- Overload dung member function:
+ Toan tu gan : =
+ Toan tu Index : []
+ Toan tu Function call : ()
+ Toan tu member selection : ->


- Member Function : Toan tu 1 ngoi (thay doi gia tri toan hang ben trai) : - (Negative) , ! ,
- Friend, Normal Function : Ko lam thay doi toan hang ben trai
*/
class Number
{
	private:
		int m_num;
	public:
		Number(){}
		Number(int num) : m_num(num){}
		void setValue(int num)
		{
			this->m_num = num;
		}

		int getValue()
		{
			return this->m_num;
		}

		//Unary Operator !,++,--,-
		bool operator!()
		{
			return !m_num; //Thay the gia tri dao voi kieu gia tri tuong ung
		}

		Number operator-()   //Negative
		{
			return Number(-m_num);
		}

		Number& operator+(Number& b)  //Toan tu Cong (Plus)
		{
			this->m_num = this->m_num + b.getValue();
			return *this; 
		}

		Number& operator++()  //prefix
		{
			++(this->m_num);
			return *this;
		}

		Number operator++(int)  //postfix
		{
			
			Number num(this->m_num);
			++(*this);
			return num;
		}

		//Binary Operator
		friend bool operator==(Number& a,Number& b);
		Number& operator=(Number& b)
		{
			this->m_num = b.getValue();
			return *this;
		}

		//IO
		friend std::ostream& operator<<(std::ostream& out,const Number& num);
		friend std::istream& operator>>(std::istream& in,Number& num);
};

bool operator==(Number& a,Number& b)
{
	if(a.getValue() == b.getValue())
		return true;
	else
		return false;
}


std::ostream& operator<<(std::ostream& out,const Number& num)
{
	out<<"Number : "<<num.m_num;
	return out;
}

std::istream& operator>>(std::istream& in,Number& num)
{
	in>>num.m_num; 
	return in;
}


int main()
{
	Number a(5);
	Number b(6);

	//a=b;


	if(a==b)
		cout<<"True"<<endl;
	else
	{
		cout<<"False"<<endl;
		//cout<<(-a).getValue()<<endl;
		cout<<(a).getValue()<<endl;
		cout<<(++a).getValue()<<endl;
		cout<<(a).getValue()<<endl;
		//cout<<(a+b).getValue()<<endl;

	}

	cin>>a;
	cout<<a;
	
	
	return 0;
}

