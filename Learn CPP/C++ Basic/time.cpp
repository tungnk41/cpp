#include <iostream>
#include <chrono> //For using Time functions
using namespace std;
using namespace std::chrono;



int a[10] = {1,4,7,3,6,5,2,8,10,9};

void swap(int& a,int& b)
{
	a = a^b;
	b = a^b;
	a = a^b;
}

void BubbleSort(int* a,int size)
{
	for(int i = 0;i<(size-1);i++){
		for(int j = 0;j<(size-i-1);j++){
			if(a[j]>a[j+1]){
				swap(a[j],a[j+1]);
			}
		}
	}
}



int main()
{
	auto start = high_resolution_clock::now();
	BubbleSort(a,10);
	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<milliseconds>(stop - start);
	//auto duration = duration_cast<nanoseconds>(stop - start);
	cout << "BubbleSort took '" << duration.count() << "' milli seconds." << endl << endl;
	return 0;
}