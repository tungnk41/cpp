#include <iostream>
#include <cstdlib>
using namespace std;

#define VECTOR_INIT_CAPACITY 100

typedef struct 
{
	int size;
	int capacity;
	int* data;
} Vector;

void Vector_Init(Vector* vector);
void Vector_Append(Vector* vector, int value);
void Vector_Set(Vector* vector, int index, int value);
int  Vector_Get(Vector* vector, int index);
void Vector_DoubleCapacity(Vector* vector);
void Vector_Free(Vector* vector);

void Vector_Init(Vector* vector)
{
	vector->size = 0;
	vector->capacity = VECTOR_INIT_CAPACITY;
	vector->data = malloc(sizeof(int)*capacity);
}

void Vector_DoubleCapacity(Vector* vector)
{
	if(vector->size >= vector->capacity)
		capacity *= 2;
	vector->data = realloc(vector->data, sizeof(int)*vector->capacity);
}

void Vector_Append(Vector* vector, int value)
{
	Vector_DoubleCapacity(vector);
	vector->data[vector->size++] = value;
}

void Vector_Set(Vector* vector, int index, int value)
{
	while(index >= vector->size)
		Vector_Append(vector,0);

	vector->data[index] = value;
}

int  Vector_Get(Vector* vector, int index)
{
	if(index > vector->size || index < 0)
	{
		std::cout<<"Index "<<index<<" out of bound for Vector of Size "<<vector->size<<std::endl;
	}

	return vector->data[index];
}

void Vector_Free(Vector* vector)
{
	free(vector->data);
}

int main(int argc, char const *argv[])
{
	/* code */
	return 0;
}