#include <iostream>
using namespace std;

void swap(int&a , int& b)
{
	a = a^b;
	b = a^b;
	a = a^b;
}


int main()
{
	int a[5][5] = {
		{1,2,3,4,5},
		{6,7,8,9,10},
		{11,12,13,14,15},
		{1,2,3,4,5},
		{6,7,8,9,10},
	};

	int i,j;

	for(i=0;i<5;i++)
		for(j=0;j<i;j++){
			swap(a[i][j],a[j][i]);
		}

	for (int i = 0; i < 5; ++i)
	{
		cout<<a[2][i]<<" "<<endl;
	}


	return 0;
}